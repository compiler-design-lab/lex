#pragma once
#include <iostream>
#include <regex>
#include <string>
#include <vector>

namespace lex {

class lex {
private:
  std::string current_line = "";
  std::string::const_iterator current_pos = current_line.cend();
  uint64_t linecount = 0;
  std::vector<std::regex> const regexes;

public:
  class Token {
  public:
    enum class Type {
      identifier = 5,
      keyword = 1,
      opera = 2,
      spchar = 3,
      constant = 4,
      preprocessor = 0,
      invalid = 7
    };
    std::string value;
    Type type;
    uint64_t line;
    uint64_t col;
  };

  Token invalid_token{"", Token::Type::invalid};

  std::vector<Token> tokens;

  Token getNextTokenFromSteam(std::istream &is);
  lex();
};
} // namespace lex
