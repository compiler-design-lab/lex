#include "lex.h"
#include <algorithm>
#include <locale>

namespace lex {

lex::lex()
    : regexes(
          {std::regex(R"(^(#.+))"),
           std::regex(
               R"(^(auto|break|case|char|const|continue|default|do|double|else|enum|extern|float|for|goto|if|int|long|register|return|short|signed|sizeof|static|struct|switch|typedef|union|unsigned|void|volatile|while))"),
           std::regex(
               R"(^((\+=)|(-=)|(\*=)|(\/=)(\%=)|(<<)|(>>)|(\|\|)|(\&\&)|(==)|(\+\+)|(--)|\?|:|=|\+|\-|\/|\%))"),
           std::regex(
               R"(^(\`|\~|\@|\!|\$|\^|\*|\%|\&|\(|\)|\[|\]|\{|\}|\<|\>|\+|\=|\_|\–|\||\/|\\|\;|\:|\'|\“|\,|\.|\?))"),
           std::regex(
               R"(^(([0-9]+\.{0,1}[0-9]*)|(\"([^\"\']*)\")|(\'([^\'\"])\')))"),
           std::regex(R"(^([^\d])([A-Z]|[a-z]|[0-9]|_)*)")}){};

std::string::const_iterator eatWhiteSpace(std::string::const_iterator begin,
                                          std::string::const_iterator end) {
  return std::find_if(begin, end, [](auto const &c) {
    return !std::isspace(c, std::locale(""));
  });
}

lex::Token lex::getNextTokenFromSteam(std::istream &is) {
  // get a starting position
  current_pos = eatWhiteSpace(current_pos, current_line.cend());
  while (current_line.cend() == current_pos) {
    ++linecount;
    if (!std::getline(is, current_line))
      return invalid_token;
    current_pos = current_line.cbegin();
    current_pos = eatWhiteSpace(current_pos, current_line.cend());
  }

  // match with the regex vector
  std::smatch matches;
  for (int i = 0; i < regexes.size(); ++i) {
    if (!std::regex_search(current_pos, current_line.cend(), matches,
                           regexes[i]))
      continue;
    uint64_t col = std::distance(current_line.cbegin(), current_pos) + 1;
    current_pos = matches[0].second;
    tokens.push_back(
        Token{matches[0].str(), static_cast<Token::Type>(i), linecount, col});
    return tokens.back();
  }
  return invalid_token;
}
} // namespace lex
