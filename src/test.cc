#include "lex.h"
#include <iostream>

int main() {
  lex::lex lexer;
  while (std::cin) {
    auto tok = lexer.getNextTokenFromSteam(std::cin);
    switch (tok.type) {
      using Types = lex::lex::Token::Type;
    case Types::identifier:
      std::cout << tok.value << " : identifier\n";
      break;
    case Types::keyword:
      std::cout << tok.value << " : keyword\n";
      break;
    case Types::opera:
      std::cout << tok.value << " : operator\n";
      break;
    case Types::spchar:
      std::cout << tok.value << " : special character\n";
      break;
    case Types::constant:
      std::cout << tok.value << " : constant\n";
      break;
    case Types::preprocessor:
      std::cout << tok.value << " : preprocessor\n";
      break;
    case Types::invalid:
      std::cout << tok.value << " : invalid\n";
      return -1;
      break;
    }
  }
}
